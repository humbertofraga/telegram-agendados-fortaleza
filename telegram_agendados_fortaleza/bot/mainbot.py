import datetime
import logging
import os
import sys

from telegram.ext import Updater, PicklePersistence, CallbackContext

from telegram_agendados_fortaleza.bot.nofitier import Notifier
from telegram_agendados_fortaleza.bot.start_conversation_handler import StartConversationHandler
from telegram_agendados_fortaleza.bot.unique_search_conversation_handler import UniqueSearchConversationHandler
from telegram_agendados_fortaleza.bot.manage_list_conversation_handler import ManageListConversationHandler
from telegram_agendados_fortaleza.lista_vacinacao.vac_list_parser import VacListParser
from telegram_agendados_fortaleza.pdf.pdf_json_converter import PDFJsonConverter
from telegram_agendados_fortaleza.util import config


class Bot:
    def __init__(self):
        logging.basicConfig(
            format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
            level=logging.INFO
        )
        self.config = config.Config()
        self._logger = logging.getLogger("agendados-fortaleza")
        self._updater = self.create_bot()
        self.list_parser = VacListParser()
        self.repeating_job = self._updater.job_queue.run_repeating(
            self.callback_job,
            interval=(15 * 60),
            first=10
        )
        self._config_handlers()

    def create_bot(self):
        data_dir = self.config.get_data_dir()
        if not os.path.exists(data_dir):
            os.mkdir(data_dir)
        elif not os.path.isdir(data_dir):
            self._logger.error(f"{data_dir} must be a directory")
            sys.exit(1)
        persistence = PicklePersistence(f"{data_dir}/persistence.pickle")
        return Updater(
            token=self.config.get_token(),
            persistence=persistence,
            use_context=True
        )

    def _config_handlers(self):
        dispatcher = self._updater.dispatcher
        dispatcher.add_handler(StartConversationHandler())
        dispatcher.add_handler(UniqueSearchConversationHandler())
        dispatcher.add_handler(ManageListConversationHandler())

    def start(self):
        self._logger.info("Starting bot")
        self._updater.start_polling()
        self._updater.idle()
        self._logger.info("Bot stopped")

    def callback_job(self, context: CallbackContext):
        new_lists = {}
        self.list_parser.load()
        vac_lists = self.list_parser.get_last_days(3)
        for day in vac_lists:
            for desc in vac_lists[day]:
                date = datetime.datetime.strptime(day, "%d/%m/%Y")
                self._logger.info(f"Extracting list {desc}")
                pdf_converter = PDFJsonConverter(date, desc, vac_lists[day][desc])
                file_saved = pdf_converter.extract_data()
                if file_saved:
                    new_lists[day] = {desc: vac_lists[day][desc]}
        if len(new_lists) > 0:
            Notifier(self.config.get_users(), new_lists, context).send_notifications()
