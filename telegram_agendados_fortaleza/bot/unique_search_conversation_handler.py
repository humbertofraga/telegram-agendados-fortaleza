import logging

from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup, ChatAction, ParseMode
from telegram.ext import ConversationHandler, CommandHandler, CallbackQueryHandler, CallbackContext, MessageHandler, \
    Filters

from telegram_agendados_fortaleza.lista_vacinacao.vac_list_parser import VacListParser
from telegram_agendados_fortaleza.pdf.pdf_scrapper import PDFScrapper

log = logging.getLogger()


class UniqueSearchConversationHandler(ConversationHandler):
    def __init__(self):
        self.STATE_SELECT_DAY = "select_day"
        self.STATE_SELECT_LIST = "select_list"
        self.STATE_TYPE_NAME = "type_name"
        self.USERDATA_DAY = "day"
        self.USERDATA_LIST = "list"
        super().__init__(
            entry_points=[CommandHandler('pesquisar', self.cmd_search)],
            states={
                self.STATE_SELECT_DAY: [CallbackQueryHandler(self.callback_select_day)],
                self.STATE_SELECT_LIST: [CallbackQueryHandler(self.callback_select_list)],
                self.STATE_TYPE_NAME: [MessageHandler(Filters.text, self.text_name)]
            },
            fallbacks=[CommandHandler('cancelar', self.cmd_cancel)]
        )
        vac_list_parser = VacListParser()
        vac_list_parser.load()
        self.list_days = vac_list_parser.get_last_days(3)

    def cmd_search(self, update: Update, context: CallbackContext):
        buttons = []
        for day in self.list_days:
            buttons.append(InlineKeyboardButton(day, callback_data=f"{day}"))
        keyboard = InlineKeyboardMarkup.from_column(buttons)
        update.effective_chat.send_message("Selecione o dia a pesquisar:",
                                           reply_markup=keyboard)
        return self.STATE_SELECT_DAY

    def callback_select_day(self, update: Update, context: CallbackContext):
        query = update.callback_query
        query.answer()
        buttons = []
        context.user_data[self.USERDATA_DAY] = query.data
        for vac_list in self.list_days[query.data]:
            buttons.append(InlineKeyboardButton(vac_list, callback_data=f"{vac_list}"))
        keyboard = InlineKeyboardMarkup.from_column(buttons)
        query.message.edit_text(f"Escolha uma lista do dia {query.data} para pesquisar:",
                                reply_markup=keyboard)
        return self.STATE_SELECT_LIST

    def callback_select_list(self, update: Update, context: CallbackContext):
        query = update.callback_query
        query.answer()
        context.user_data[self.USERDATA_LIST] = query.data
        query.message.edit_text(f"Lista {query.data}.\n"
                                "Digite o nome a pesquisar.\n"
                                "Fique atento a erros de digitação.\n"
                                "Acentos serão desconsiderados.")
        return self.STATE_TYPE_NAME

    def text_name(self, update: Update, context: CallbackContext):
        day = context.user_data[self.USERDATA_DAY]
        vac_list = context.user_data[self.USERDATA_LIST]
        url = self.list_days[day][vac_list]
        update.effective_chat.send_chat_action(ChatAction.TYPING)
        pdf_getter = PDFScrapper(day, vac_list, url)
        result = pdf_getter.search_string(update.message.text)
        if len(result) == 0:
            update.effective_chat.send_message("Nenhum resultato encontrado")
        else:
            update.effective_chat.send_message("Resultados encontrados:")
            for line in result:
                update.effective_chat.send_message(f"{line[0]}\n"
                                                   f"<b>Data Nascimento:</b> {line[1]}\n"
                                                   f"<b>Local:</b> {line[2]}\n"
                                                   f"<b>Data e hora:</b> {line[3]} {line[4]}\n"
                                                   f"<b>Dose:</b> {line[5]}ª",
                                                   parse_mode=ParseMode.HTML)
        return self.END

    def cmd_cancel(self, update: Update, context: CallbackContext):
        context.user_data.pop(self.USERDATA_DAY)
        context.user_data.pop(self.USERDATA_LIST)
        return self.END
