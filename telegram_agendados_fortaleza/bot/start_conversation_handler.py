import logging

from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CommandHandler, CallbackContext, CallbackQueryHandler
from telegram_agendados_fortaleza.util import config

log = logging.getLogger()


class StartConversationHandler(ConversationHandler):
    def __init__(self):
        self.STATE_SAVE_PERMISSION = "save_permission"
        super().__init__(
            entry_points=[CommandHandler('start', self.cmd_start)],
            states={
                self.STATE_SAVE_PERMISSION: [CallbackQueryHandler(self.callback_query_save_data)]
            },
            fallbacks=[CommandHandler('cancelar', self.cmd_cancel)]
        )
        self.config = config.Config()

    def cmd_start(self, update: Update, context: CallbackContext):
        update.effective_chat.send_message("Olá! Eu pesquiso nomes nas listas de agendamento de vacinas da cidade de "
                                           "Fortaleza/CE.")
        inline_buttons = [
            InlineKeyboardButton("Sim", callback_data="Sim"),
            InlineKeyboardButton("Não", callback_data="Não")
        ]
        keyboard = InlineKeyboardMarkup.from_row(inline_buttons)
        update.effective_chat.send_message("Posso armazenar seus dados do Telegram para facilitar nossa comunicação no futuro?",
                                           reply_markup=keyboard)
        return self.STATE_SAVE_PERMISSION

    def callback_query_save_data(self, update: Update, context: CallbackContext):
        query = update.callback_query
        query.answer()
        query.edit_message_reply_markup(None)
        if query.data == "Sim":
            log.info(f"User {update.effective_user.id} choose {query.data}")
            query.message.reply_text("Ok, vou salvar seu contato.")
            self.config.save_user(update.effective_user.id)
        else:
            log.info(f"Will not save data for user {update.effective_user.id}")
            query.message.reply_text("Certo, não vou salvar nada.")
        return self.END

    def cmd_cancel(self, update, context):
        return self.END
