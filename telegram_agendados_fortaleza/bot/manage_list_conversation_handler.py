import logging

from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CommandHandler, CallbackContext, CallbackQueryHandler, MessageHandler, \
    Filters

from telegram_agendados_fortaleza.util.config import Config

log = logging.getLogger("ManageList")


class ManageListConversationHandler(ConversationHandler):
    def __init__(self):
        self.STATE_SELECT_ACTION = "action"
        self.STATE_SHOW = "show"
        self.STATE_ADD = "add"
        self.STATE_REMOVE = "remove"
        self.ACTION_SHOW = "act_show"
        self.ACTION_ADD = "act_add"
        self.ACTION_REMOVE = "act_remove"
        super().__init__(
            entry_points=[CommandHandler('lista', self.cmd_entry)],
            states={
                self.STATE_SELECT_ACTION: [CallbackQueryHandler(self.callback_action)],
                self.STATE_ADD: [MessageHandler(Filters.text, self.add_name)],
                self.STATE_REMOVE: [CallbackQueryHandler(self.callback_remove)]
            },
            fallbacks=[CommandHandler('cancelar', self.cmd_cancel)]
        )
        self.config = Config()

    def cmd_entry(self, update: Update, context: CallbackContext):
        user_id = update.effective_user.id
        user = self.config.get_user(user_id)
        if user is None:
            return self.user_not_found(update, context)
        else:
            update.effective_chat.send_message(
                "Posso guardar nomes para pesquisar sempre que uma nova lista for publicada.\n"
                "Digite /cancelar para cancelar"
            )
            return self.list_actions(update, context)

    def list_actions(self, update: Update, context: CallbackContext):
        inline_buttons = [
            [
                InlineKeyboardButton("Ver a lista", callback_data=self.ACTION_SHOW)
            ],
            [
                InlineKeyboardButton("Adicionar nome", callback_data=self.ACTION_ADD),
                InlineKeyboardButton("Remover nome", callback_data=self.ACTION_REMOVE)
            ]
        ]
        update.effective_chat.send_message(
            "Escolha uma ação:",
            reply_markup=InlineKeyboardMarkup(inline_buttons)
        )
        return self.STATE_SELECT_ACTION

    def callback_action(self, update: Update, context: CallbackContext):
        query = update.callback_query
        query.answer()
        # query.edit_message_reply_markup(None)
        user_id = update.effective_user.id
        user = self.config.get_user(user_id)
        if user is None:
            return self.user_not_found(update, context)
        if query.data == self.ACTION_SHOW:
            if 'names' not in user or len(user['names']) <= 0:
                query.message.edit_text("A lista está vazia")
                return self.list_actions(update, context)
            else:
                query.message.edit_text("Lista de nomes cadastrados:")
                for name in user['names']:
                    update.effective_chat.send_message(name)
                return self.END
        elif query.data == self.ACTION_ADD:
            query.message.edit_text("Informe o nome a adicionar à lista:")
            return self.STATE_ADD
        elif query.data == self.ACTION_REMOVE:
            if len(user["names"]) <= 0:
                query.message.edit_text("A lista está vazia")
                return self.list_actions(update, context)
            inline_buttons = []
            for name in user["names"]:
                inline_buttons.append([InlineKeyboardButton(name, callback_data=name)])
            query.message.edit_text(
                "Vou mostrar os nomes cadastrados. Selecione um para remover da lista:",
                reply_markup=InlineKeyboardMarkup(inline_buttons)
            )
            return self.STATE_REMOVE

    def add_name(self, update: Update, context: CallbackContext):
        name = update.message.text
        user_id = update.effective_user.id
        user = self.config.get_user(user_id)
        if user is None:
            return self.user_not_found(update, context)
        self.config.add_name(user_id, name)
        update.effective_chat.send_message(
            "Nome adicionado"
        )
        return self.END

    def callback_remove(self, update: Update, context: CallbackContext):
        query = update.callback_query
        query.answer()
        query.edit_message_reply_markup(None)
        user_id = update.effective_user.id
        user = self.config.get_user(user_id)
        if user is None:
            return self.user_not_found(update, context)
        if len(user["names"]) <= 0:
            query.message.edit_text("A lista está vazia")
            return self.list_actions(update, context)
        self.config.remove_name(user_id, query.data)
        query.message.edit_text("Nome removido")
        return self.END

    def user_not_found(self, update: Update, context: CallbackContext):
        try:
            update.callback_query.edit_message_reply_markup(None)
        except AttributeError:
            pass
        update.effective_chat.send_message("Não tenho seu contato salvo.\n"
                                           "Use o comando /start para permitir salvar o seu contato.")
        return self.END

    def cmd_cancel(self, update: Update, context: CallbackContext):
        return self.END
