from telegram import ParseMode
from telegram.ext import CallbackContext

from telegram_agendados_fortaleza.pdf.pdf_scrapper import PDFScrapper


class Notifier:
    def __init__(self, users: list, lists, context: CallbackContext):
        self.users = users
        self.lists = lists
        self.context = context

    def send_notifications(self):
        for user in self.users:
            self.context.bot.send_message(
                chat_id=user,
                text="Novas listas disponíveis!"
            )
        for day in self.lists:
            for desc in self.lists[day]:
                pdf_scrapper = PDFScrapper(day, desc, self.lists[day][desc])
                for user in self.users:
                    self.context.bot.send_message(
                        chat_id=user,
                        text=f"Dia {day}, {desc}"
                    )
                    for name in self.users[user]['names']:
                        results = pdf_scrapper.search_string(name)
                        if len(results) == 0:
                            self.context.bot.send_message(
                                chat_id=user,
                                text=f"\"{name}\" não foi encontrado"
                            )
                        else:
                            for result in results:
                                self.context.bot.send_message(
                                    chat_id=user,
                                    text=f"{result[0]}\n"
                                                   f"<b>Data Nascimento:</b> {result[1]}\n"
                                                   f"<b>Local:</b> {result[2]}\n"
                                                   f"<b>Data e hora:</b> {result[3]} {result[4]}\n"
                                                   f"<b>Dose:</b> {result[5]}ª",
                                                   parse_mode=ParseMode.HTML
                                )
