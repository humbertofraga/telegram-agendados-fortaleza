import datetime
import re

from requests_html import HTMLSession


class NotReadyException(Exception):
    pass


class VacListParser:
    def __init__(self):
        self.url = "https://coronavirus.fortaleza.ce.gov.br/listaVacinacao.html"
        self.session = HTMLSession()
        self.session.browser
        self.resp = None

    def load(self):
        self.resp = self.session.get(self.url)
        self.resp.html.render(sleep=0.5)

    def get_list(self):
        if self.resp is None:
            raise NotReadyException("Object is not ready, call load() first")
        return self.resp.html.find("a.text-info")

    def get_last_days(self, num_days=0):
        response = {}
        today = datetime.date.today()
        delta = datetime.timedelta(days=num_days)
        oldest = today - delta
        re_desc_date = re.compile(r"(.*) - .*([0-9]{2}/[0-9]{2}/[0-9]{4})")
        for line in self.get_list():
            desc_date = re_desc_date.match(line.text)
            if desc_date is not None:
                date = desc_date[2]
                try:
                    d_date = datetime.datetime.strptime(date, "%d/%m/%Y")
                except ValueError:
                    continue
                if d_date.date() < oldest:
                    continue
                desc = desc_date[1]
                if date not in response.keys():
                    response[date] = {desc: line.attrs['href']}
                else:
                    response[date][desc] = line.attrs['href']
        return response


if __name__ == '__main__':
    vac = VacListParser()
    vac.load()
    last_n = vac.get_last_days(3)
    for day in last_n:
        print(day)
