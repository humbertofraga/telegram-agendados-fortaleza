import json
import logging

_FILE_PATH = 'config.json'
_TOKEN = 'token'
_HEROKU_URL = 'heroky_url'
_ADMIN = 'admin'
_DATA_DIR = 'data_dir'
_USERS = 'users'


class Config:
    def __init__(self):
        self.log = logging.getLogger("Config")
        self._data = None
        self.load_config()

    def load_config(self):
        try:
            with open(_FILE_PATH, 'r') as file:
                self._data = json.load(file)
        except (FileNotFoundError, AttributeError):
            self._data = None
            self.init()

    def init(self):
        if self._data is None:
            self._data = {
                _TOKEN: 'TELEGRAM_BOT_TOKEN',
                _HEROKU_URL: '',
                _ADMIN: ["USER_ID"],
                _DATA_DIR: 'data',
                _USERS: []
            }
        self.commit()

    def commit(self):
        with open(_FILE_PATH, 'w') as file:
            json.dump(self._data, file, indent=2)
            file.close()

    def get_token(self):
        self.load_config()
        return self._data.get(_TOKEN)

    def set_token(self, token):
        self.load_config()
        self._data[_TOKEN] = token
        self.commit()

    def is_admin(self, user_id):
        self.load_config()
        return user_id in self._data.get(_ADMIN)

    def get_data_dir(self):
        self.load_config()
        return self._data.get(_DATA_DIR)

    def get_heroku_url(self):
        self.load_config()
        return self._data.get(_HEROKU_URL)

    def set_heroku_url(self, url):
        self.load_config()
        self._data[_HEROKU_URL] = url
        self.commit()

    def save_user(self, user_id):
        self.load_config()
        if self.get_user(user_id) is not None:
            return
        self._data[_USERS][user_id] = {}
        self.commit()

    def get_users(self):
        self.load_config()
        return self._data.get(_USERS)

    def get_user(self, user_id):
        self.load_config()
        uid = f"{user_id}"
        if uid in self._data.get(_USERS):
            return self._data.get(_USERS)[uid]
        else:
            return None

    def add_name(self, user_id, name):
        self.load_config()
        user = self.get_user(user_id)
        if user is not None:
            if 'names' not in user:
                user['names'] = [name]
            else:
                names = user['names']
                if name not in names:
                    names.append(name)
            self.commit()
            return True
        else:
            return False

    def remove_name(self, user_id, name):
        self.load_config()
        user = self.get_user(user_id)
        if user is not None:
            user['names'].remove(name)
            self.commit()
            return True
        else:
            return False
