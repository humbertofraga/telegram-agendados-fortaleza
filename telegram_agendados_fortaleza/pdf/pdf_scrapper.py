import datetime
import json
import logging
import os
import re

from telegram_agendados_fortaleza.pdf.pdf_json_converter import PDFJsonConverter
from telegram_agendados_fortaleza.util import config

log = logging.getLogger()


class PDFScrapper:
    def __init__(self, day, description, url):
        self.config = config.Config()
        self.date = datetime.datetime.strptime(day, "%d/%m/%Y")
        self._cache_dir = f"{self.config.get_data_dir()}/cache/{self.date.strftime('%Y-%m-%d')}"
        self._data_file = None
        self._data = None
        self.description = description
        self.url = url
        self.pdf_getter = PDFJsonConverter(self.date, self.description, self.url)

    def open(self):
        data_path = f"{self._cache_dir}/{self.description}.json"
        if not os.path.exists(data_path):
            self.pdf_getter.extract_data()
        self._data_file = open(data_path, 'r')
        self._data = json.loads(self._data_file.read())

    def close(self):
        self._data_file.close()

    def search_string(self, key):
        if self._data is None:
            self.open()
        results = []
        re_key = re.compile(key.upper())
        for line in self._data:
            if re_key.search(line[0]) is not None:
                results.append(line)
        self.close()
        return results
