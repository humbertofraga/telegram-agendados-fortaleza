import json
import logging
import os
import re
import sys

import camelot
import requests

from telegram_agendados_fortaleza.util import config

log = logging.getLogger("pdf_getter")
logging.getLogger("camelot").setLevel(logging.ERROR)
logging.getLogger("pdfminer.pdfdocument").setLevel(logging.ERROR)
logging.getLogger("pdfminer.pdfinterp").setLevel(logging.ERROR)
logging.getLogger("pdfminer.pdfpage").setLevel(logging.ERROR)


class PDFJsonConverter:
    def __init__(self, date, description, url):
        log.info(f"Getter for url {url}")
        self.config = config.Config()
        self.description = description
        self.url = url
        self._cache_dir = f"{self.config.get_data_dir()}/cache/{date.strftime('%Y-%m-%d')}"
        self._pdf_path = f"{self._cache_dir}/{description}.pdf"
        self._json_path = f"{self._cache_dir}/{description}.json"

    def _create_cache_dir(self):
        path = self._cache_dir
        if not os.path.exists(path):
            os.makedirs(path, exist_ok=True)
        elif not os.path.isdir(path):
            log.error(f"{path} must be a directory")
            sys.exit(1)

    def get_file(self):
        self._create_cache_dir()
        if os.path.exists(self._pdf_path):
            return
        log.info("Downloading file")
        # url can be one of:
        # - https://ms.dados.sms.fortaleza.ce.gov.br/220521TERCEIRAFASED1.pdf
        # - https://drive.google.com/file/d/1QMeRt2TSX9qp7W6TjWRmEigYR5Ma6NBG/view
        # On the second case, we must convert to
        # https://drive.google.com/uc?export=download&id=1QMeRt2TSX9qp7W6TjWRmEigYR5Ma6NBG
        file_id = re.match(r"https://drive\.google\.com/file/d/(.*)/view(.*)?", self.url)
        try:
            self.url = f"https://drive.google.com/uc?export=download&id={file_id.group(1)}"
        except AttributeError:
            pass
        log.info(self.url)
        response = requests.get(self.url)
        with open(self._pdf_path, 'wb') as file:
            file.write(response.content)

    def extract_data(self):
        """
        Extract data from PDF files, writing it to JSON files.
        :return: True if new files are created, False when no new file is created
        """
        if os.path.exists(self._json_path):
            return False
        if not os.path.exists(self._pdf_path):
            self.get_file()
        dose = re.search(r"D.$", self.description)[0]
        log.info(f"This is a list for {dose}")
        full_table = []
        log.info("Taking file to camelot")
        tables = camelot.read_pdf(self._pdf_path, pages="all", flavor="stream", edge_tol=50)
        if dose == "D1":
            full_table = self.line_d1(tables)
        elif dose == "D2":
            full_table = self.line_d2(tables)
        with open(self._json_path, 'w') as file:
            file.write(json.dumps(full_table, indent=2))
            file.close()
            return True

    def line_d1(self, tables):
        """
        Tries to correctly divide each line in the following columns:
        NOME DATA_NASCIMENTO LOCAL_DE_VACINAÇÃO DATA HORA DOSE
        :param tables:
        :return:
        """
        result = []
        re_birth_local = re.compile(r"([0-9]{4}-[0-9]{2}-[0-9]{2}) ([A-Z ]*)")
        for table in tables:
            log.info(table)
            for line in table.data:
                # For the most lines, the cells 1 or 2 will be empty while the other
                # will contain both content concatenated. This is because on the PDF
                # column 1 is right-aligned and column 2 is left-aligned
                if line[0] == "":
                    continue
                try:
                    birth_local = re_birth_local.match(line[1] + line[2])
                    if birth_local is not None:
                        line[1] = birth_local[1]
                        line[2] = birth_local[2]
                    log.info(line)
                    result.append(line)
                except IndexError:
                    continue
        return result

    def line_d2(self, tables):
        """
        Tries to correctly divide each line in the following columns:
        nome data_nascimento local_vacinacao data hora dose data_da_dose1
        :param tables:
        :return:
        """
        result = []
        re_dose1_data = re.compile(r"([0-9]) ([0-9]{4}-[0-9]{2}-[0-9]{2})")
        for table in tables:
            # log.info(f"Table in page {table.page} with {len(table.rows)} rows")
            for line in table.data:
                if line[0] == "":
                    continue
                try:
                    dose1_data = re_dose1_data.match(line[5])
                    if dose1_data is not None:
                        line[5] = dose1_data[1]
                        line.append(dose1_data[2])
                    log.info(line)
                    result.append(line)
                except IndexError:
                    continue
        return result
