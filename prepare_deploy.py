from os import environ
from telegram_agendados_fortaleza.util import config

TELEGRAM_TOKEN = environ.get('TELEGRAM_BOT_TOKEN')
HEROKU_URL = f"https://{environ.get('HEROKU_APP')}.herokuapp.com/"

c = config.Config()
c.set_token(TELEGRAM_TOKEN)
c.set_heroku_url(HEROKU_URL)
