from setuptools import setup, find_packages

setup(
    name='telegram_agendados_fortaleza',
    version='1.0',
    packages=find_packages(),
    url='',
    license='Apache-2.0',
    author='Humberto Fraga',
    author_email='humbertofraga@gmail.com',
    description='Bot Telegram para pesquisar nomes nas listas de agendamentos de vacinação de Fortaleza/CE',
    entry_points={
        'console_scripts': [
            'agendados-fortaleza=telegram_agendados_fortaleza.main:main'
        ]
    },
    install_requires=[i.strip() for i in open("requirements.txt").readlines()]
)
