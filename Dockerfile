FROM python:3-slim

RUN apt-get update && \
    apt-get install -y python3-opencv

WORKDIR /app

COPY . .

RUN pip3 install .

CMD ["agendados-fortaleza"]
